	# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2020-03-13
### Added
- Add LineRenderer follow position and rotation

## [1.1.0] - 2020-01-26
### Added
- Add Translator component

### Changed
- Fix GetEnabledMonoBehavior extension method

## [1.0.1] - 2020-01-25
### Added
- Add Unit Tests for extension methods
- Improve code documentation

### Changed
- Fix and improve FindChildComponent
- Fix GetOrAddComponent

## [1.0.0] - 2020-01-18
### Added
- Release initial version
- Add TransformExtension
- Add CHANGELOG
- Add README
- Add initial files
- Initial commit