# Transform Extension

### What is this package for? ###

* Code extensions for Unity Transform class
* Minimum Unity version: 2018.3

### What can I do with it? ###
* You can use several methods to **find** components both in the **Game Object** or **Scene** hierarchy using **Transform** class method extension
	* ```transform.FindChildComponent<AudioSource>("jump");``` : finds the first child *AudioSource* component by the given *jump* name.
	* ```transform.Find<Rigidbody>("Player");``` : searches on entire scene for the first *Rigidbody* component and the given *Player* name.
* Just install the package and start to use those methods.


### How do I get set up? ###
* You can download this repo and place it inside your Unity project (the simplest way).
* Using **Unity Package Manager**:
	* Open the **manifest.json** file inside your Unity project's **Packages** folder;
	* For *versions 2018.3* or above, there are two options:
		* Using the **Package Registry Server**:
			* Add this line before *"dependencies"* node:
				* ```"scopedRegistries": [ { "name": "Action Code", "url": "http://34.83.179.179:4873/", "scopes": [ "com.actioncode" ] } ],```
			* The package **ActionCode-TransformExtension** will be available for you to install using the **Package Manager** windows.
		* By **Git URL** (you'll need a **Git client** installed on your machine):
			* Add this line inside *"dependencies"* node: 
				* ```"com.actioncode.transform-extension":"https://bitbucket.org/nostgameteam/transformextension.git"```

	* For *versions 2017.2* or below: 
		* Clone/download this repo in any folder on your machine;
		* Add this line inside *"dependencies"* node: 
			* ```"com.actioncode.transform-extension": "[the-folder-path-you-download-it]"```

### Who do I talk to? ###

* Repo owner and admin: **Hyago Oliveira** (hyagogow@gmail.com)