﻿using UnityEngine;

namespace ActionCode.TransformExtension
{
    /// <summary>
    /// Follows a LineRenderer component in some way.
    /// </summary>
	[ExecuteInEditMode]
	public abstract class AbstractFollowLine : MonoBehaviour 
	{
        [SerializeField, Tooltip("The LineRenderer component.")]
        protected LineRenderer line;        

        protected virtual void Reset()
        {
            line = 
                GetComponentInParent<LineRenderer>() ?? 
                GetComponentInChildren<LineRenderer>();
        }

        private void Update () 
		{
            Follow();
        }

        private void OnValidate()
        {
            if (line) ValidateFields();
        }

        /// <summary>
        /// Clamps the given index between 0 and the line renderer position count.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>An integer clamped [0 - line.positionCount] </returns>
        public int ClampToPosition(int index)
        {
            return Mathf.Clamp(index, 0, line.positionCount - 1);
        }

        protected abstract void Follow();		
        protected abstract void ValidateFields();
	}
}