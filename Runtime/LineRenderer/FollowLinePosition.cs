﻿using UnityEngine;

namespace ActionCode.TransformExtension
{
    /// <summary>
    /// Follows a given LineRenderer position index. 
    /// </summary>
    public sealed class FollowLinePosition : AbstractFollowLine
    {
        [SerializeField, Min(0), Tooltip("The LineRenderer position index to follow.")]
        private int index = 0;

        protected override void Follow()
        {
            transform.localPosition = line.GetPosition(index);
        }

        protected override void ValidateFields()
        {
            index = ClampToPosition(index);
        }
    }
}