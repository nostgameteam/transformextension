﻿using UnityEngine;

namespace ActionCode.TransformExtension
{
    /// <summary>
    /// Rotates towards the LineRenderer look at position index.
    /// </summary>
    public sealed class FollowLineRotation : AbstractFollowLine
    {
        [Tooltip("Check if this GameObject represents a 2D.")]
        public bool is2D = false;
        [SerializeField, Min(0), Tooltip("The LineRenderer position index to rotates towards.")]
        private int lookToIndex = 0;        

        protected override void Reset()
        {
            base.Reset();
            is2D = GetComponent<SpriteRenderer>();
        }

        protected override void Follow()
        {
            Vector3 lookWorldPosition = line.transform.position + line.GetPosition(lookToIndex);
            if (is2D) transform.LookAt2D(lookWorldPosition);
            else transform.LookAt(lookWorldPosition);            
        }

        protected override void ValidateFields()
        {
            lookToIndex = ClampToPosition(lookToIndex);
        }
    }
}