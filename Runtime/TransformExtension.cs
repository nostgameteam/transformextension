﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Static class for Transform code extension.
/// </summary>
public static class TransformExtension
{
    /// <summary>
    /// Resets the transform position, localRotation and localScale to zero.
    /// </summary>
    /// <param name="transform"></param>
    public static void ResetTransformation(this Transform transform)
    {
        transform.position = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Get or adds the given component.
    /// </summary>
    /// <typeparam name="T">The type of component</typeparam>
    /// <param name="transform"></param>
    /// <returns>The component you need.</returns>
    public static T GetOrAddComponent<T>(this Transform transform) where T : Component
    {
        T component = transform.GetComponent<T>();
        if (component) return component;
        return transform.gameObject.AddComponent<T>();
    }

    /// <summary>
    /// Returns the first enabled component of the given type in the GameObject or any of its children using depth first search.
    /// </summary>
    /// <typeparam name="T">The type of component</typeparam>
    /// <param name="transform"></param>
    /// <returns>A component of the matching type, if found.</returns>
    public static T GetEnabledMonoBehavior<T>(this Transform transform) where T : MonoBehaviour
    {
        foreach (T component in transform.GetComponentsInChildren<T>(true))
        {
            if (component.enabled) return component;
        }

        return null;
    }

    /// <summary>
    /// Finds the first child transform containing the given name.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="name">The name of the child to be found</param>
    /// <param name="includeInactives">Default to true</param>
    /// <returns>The child or null</returns>
    public static Transform FindContaining(this Transform transform, string name, bool includeInactives = true)
    {
        List<Transform> childrenList = new List<Transform>(transform.GetComponentsInChildren<Transform>(includeInactives));
        int index = childrenList.FindIndex(trans => trans.gameObject.name.ToUpper().Contains(name.ToUpper()));
        if (index < 0) return null;
        return childrenList[index];
    }

    /// <summary>
    /// Finds the children transform containing the given name.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="name">The name of the children to be found</param>
    /// <returns>The children or null</returns>
    public static Transform[] FindChildrenContaining(this Transform transform, string name, bool includeInactives = true)
    {
        return System.Array.FindAll(transform.GetComponentsInChildren<Transform>(includeInactives), t => t.name.ToUpper().Contains(name.ToUpper()));
    }

    /// <summary>
    /// Searches on entire scene for a component of the given generic type.
    /// </summary>
    /// <typeparam name="T">The type of component.</typeparam>
    /// <param name="includeResources">If true, will search on resources as well.</param>
    /// <returns>The first component of generic type.</returns>
    public static T Find<T>(this Transform t, bool includeResources = false)
    {
        Transform[] transforms = includeResources ?
            Resources.FindObjectsOfTypeAll<Transform>() :
            GameObject.FindObjectsOfType<Transform>();
        foreach (Transform trans in transforms)
        {
            T typeComponent = trans.GetComponent<T>();
            if (typeComponent != null) return typeComponent;
        }

        return default;
    }

    /// <summary>
    /// Searches on entire scene for the first component of the given generic type and name.
    /// </summary>
    /// <typeparam name="T">Type of component to look for.</typeparam>
    /// <param name="name">Name of GameObject to look for.</param>
    /// <returns></returns>
    public static T Find<T>(this Transform t, string name)
    {
        GameObject gameObject = GameObject.Find(name);
        if (gameObject) return gameObject.GetComponent<T>();

        return default;
    }

    /// <summary>
    /// Searches on entire scene for all components of the given generic type.
    /// </summary>
    /// <typeparam name="T">The type of component.</typeparam>
    /// <param name="includeResources">If true, will search on resources as well.</param>
    /// <returns>A list of components of generic type.</returns>
    public static T[] FindAll<T>(this Transform t, bool includeResources = false)
    {
        List<T> typeList = new List<T>();
        Transform[] transforms = includeResources ?
            Resources.FindObjectsOfTypeAll<Transform>() :
            GameObject.FindObjectsOfType<Transform>();

        foreach (Transform trans in transforms)
        {
            T[] typeComponents = trans.GetComponents<T>();
            for (int i = 0; i < typeComponents.Length; i++)
            {
                typeList.Add(typeComponents[i]);
            }
        }

        return typeList.ToArray();
    }

    /// <summary>
    /// Looks for a child GameObject by the given path and component type.
    /// </summary>
    /// <typeparam name="T">Type of component to look for.</typeparam>
    /// <param name="path">Path of GameObject to look for. Leave it blank for looking on the root or use '/' to navigate through the GameObject hierarchy.</param>
    /// <returns></returns>
    public static T FindChild<T>(this Transform transform, string path)
    {
        Transform child = transform.Find(path);
        return child != null ?
            child.GetComponent<T>() : default;
    }

    /// <summary>
    /// Looks for a child GameObject by the given name and component type.
    /// <para>You the component is not found and you set createIfNotFound to true, a child GameObject will be created fallowing the given path.</para>
    /// </summary>
    /// <typeparam name="T">The type of component</typeparam>
    /// <param name="transform"></param>
    /// <param name="path">Path of GameObject to look for. Leave it blank for looking on the root or use '/' to navigate through the GameObject hierarchy.</param>
    /// <param name="createIfNotFound">Should create the component if not found?</param>
    /// <returns></returns>
    public static T FindChild<T>(this Transform transform, string path, bool createIfNotFound) where T : Component
    {
        T component = null;
        Transform child = transform.Find(path);
        if (child) component = child.GetComponent<T>();

        if (component == null && createIfNotFound)
        {
            if (child == null)
            {
                string[] childNames = path.Split('/');
                GameObject[] children = new GameObject[childNames.Length];
                for (int i = 0; i < children.Length; i++)
                {
                    children[i] = new GameObject(childNames[i]);
                    if (i > 0) children[i].transform.parent = children[i - 1].transform;
                }

                if (children.Length > 0)
                {
                    children[0].transform.parent = transform;
                    child = children[children.Length - 1].transform;
                }
            }
            component = child.gameObject.AddComponent<T>();
        }

        return component;
    }

    /// <summary>
    /// Rotates the transform so the right vector points at targetPosition.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="targetPosition">Point to look at.</param>
    /// <param name="space">The coordinate space in which to operate. If set to World, it will flip the transform horizontally.</param>
    public static void LookAt2D(this Transform transform, Vector3 targetPosition, Space space = Space.Self)
    {
        Vector3 direction = (targetPosition - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        bool shouldFlipHorizontally = space == Space.World && direction.x < 0f;
        if (shouldFlipHorizontally) transform.Rotate(Vector3.right, -180f, Space.Self);
    }

    /// <summary>
    /// Rotates the transform to look at the given direction on 2D way.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="direction">Direction to look at.</param>
    public static void LookAtDirection2D(this Transform transform, Vector3 direction)
    {
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}