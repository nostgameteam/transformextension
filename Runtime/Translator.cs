﻿using UnityEngine;

namespace ActionCode.TransformExtension
{
    /// <summary>
    /// Translates the GameObject by a speed in Update.
    /// </summary>
    public sealed class Translator : MonoBehaviour
    {
        [Tooltip("The speed to move."), Min(0f)]
        public float speed = 10f;
        [Tooltip("The direction to move.")]
        public Vector3 direction = Vector3.forward;
        [Tooltip("The relative space to move.")]
        public Space space = Space.Self;

        /// <summary>
        /// Current velocity
        /// </summary>
        public Vector3 Velocity { get; private set; }

        private void Update()
        {
            Velocity = direction * speed * Time.deltaTime;
            transform.Translate(Velocity, space);
        }

        /// <summary>
        /// Sets the speed and direction to translate.
        /// </summary>
        /// <param name="speed">The speed to move. Cannot be negative.</param>
        /// <param name="direction">The direction to move.</param>
        public void SetSpeedAndDirection(float speed, Vector3 direction)
        {
            this.speed = Mathf.Max(speed, 0f, speed);
            this.direction = direction;
        }
    }
}