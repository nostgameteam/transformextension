﻿using UnityEngine;
using NUnit.Framework;

namespace ActionCode.TransformExtension.Tests
{
    internal class TransformExtensionTests
    {
        private Transform transform;

        [SetUp]
        public void SetUp()
        {
            transform = new GameObject("TestGameObject").transform;
            transform.gameObject.SetActive(true);
        }

        [TearDown]
        public void TearDown()
        {
            Object.Destroy(transform.gameObject);
        }

        [Test]
        public void ResetTransformation_Resets_AllTransformComponents()
        {
            transform.position = new Vector3(10f, -50f, 35f);
            transform.localEulerAngles = new Vector3(30f, 90f, -180f);
            transform.localScale = new Vector3(2f, 3f, 4f);

            transform.ResetTransformation();

            Assert.AreEqual(transform.position, Vector3.zero);
            Assert.AreEqual(transform.rotation, Quaternion.identity);
            Assert.AreEqual(transform.localScale, Vector3.one);
        }

        [Test]
        public void GetEnabledMonoBehavior_Gets_TheOnlyEnabledMonoBehaviour()
        {
            const int enabledTranlatorIndex = 2;
            Translator[] translators = new Translator[]
            {
                transform.gameObject.AddComponent<Translator>(),
                transform.gameObject.AddComponent<Translator>(),
                transform.gameObject.AddComponent<Translator>(),
                transform.gameObject.AddComponent<Translator>()
            };

            for (int i = 0; i < translators.Length; i++)
            {
                translators[i].enabled = false;
            }
            translators[enabledTranlatorIndex].enabled = true;

            Translator foundMonoBehaviour = transform.GetEnabledMonoBehavior<Translator>();

            Assert.AreSame(foundMonoBehaviour, translators[enabledTranlatorIndex]);
        }

        [Test]
        public void GetOrAddComponent_Gets_AnExistingComponent()
        {
            AudioSource addedComponent = transform.gameObject.AddComponent<AudioSource>();
            AudioSource foundComponent = transform.GetOrAddComponent<AudioSource>();

            UnityEngine.Assertions.Assert.IsNotNull(foundComponent);
            Assert.AreSame(addedComponent, foundComponent);
        }

        [Test]
        public void GetOrAddComponent_Adds_ANewComponent()
        {
            AudioSource addedAudioSource = transform.GetOrAddComponent<AudioSource>();

            UnityEngine.Assertions.Assert.IsNotNull(addedAudioSource);
        }

        [Test]
        public void FindContaining_Finds_AnEnabledChildByAParcialName()
        {
            const string nameChildToLookFor = "IAmAChild-PleaseFindMe";

            GameObject grandparent = new GameObject("Grandparent");
            GameObject parent = new GameObject("Parent");
            GameObject child = new GameObject(nameChildToLookFor);

            child.transform.parent = parent.transform;
            parent.transform.parent = grandparent.transform;
            grandparent.transform.parent = transform;

            child.SetActive(true);
            parent.SetActive(true);
            grandparent.SetActive(true);

            int substringLength = Mathf.FloorToInt(nameChildToLookFor.Length / 2);
            string nameToLookFor = nameChildToLookFor.Substring(0, substringLength);
            Transform childFound = transform.FindContaining(nameToLookFor, false);

            Assert.AreSame(childFound, child.transform);

            Object.Destroy(grandparent);
            Object.Destroy(parent);
            Object.Destroy(child);
        }

        [Test]
        public void FindContaining_Finds_ADisabledChildByAParcialName()
        {
            const string nameChildToLookFor = "IAmAChild-PleaseFindMe";

            GameObject grandparent = new GameObject("Grandparent");
            GameObject parent = new GameObject("Parent");
            GameObject child = new GameObject(nameChildToLookFor);

            child.SetActive(false);
            parent.SetActive(false);
            grandparent.SetActive(false);

            child.transform.parent = parent.transform;
            parent.transform.parent = grandparent.transform;
            grandparent.transform.parent = transform;

            int substringLength = Mathf.FloorToInt(nameChildToLookFor.Length / 2);
            string nameToLookFor = nameChildToLookFor.Substring(0, substringLength);
            Transform childFound = transform.FindContaining(nameToLookFor, true);

            Assert.AreSame(childFound, child.transform);

            Object.Destroy(grandparent);
            Object.Destroy(parent);
            Object.Destroy(child);
        }

        [Test]
        public void FindChildrenContaining_FindsAll_EnabledChildrenByAParcialName()
        {
            const string nameChildToLookFor = "IAmAChild-";

            GameObject grandparent = new GameObject("Grandparent");
            GameObject parent = new GameObject("Parent");
            GameObject[] children = new GameObject[]
            {
                new GameObject(nameChildToLookFor + "00"),
                new GameObject(nameChildToLookFor + "01"),
                new GameObject(nameChildToLookFor + "02"),
            };

            foreach (GameObject child in children)
            {
                child.SetActive(true);
                child.transform.parent = parent.transform;
            }
            parent.transform.parent = grandparent.transform;
            grandparent.transform.parent = transform;

            parent.SetActive(true);
            grandparent.SetActive(true);

            Transform[] childrenFound = transform.FindChildrenContaining(nameChildToLookFor, false);

            Assert.AreEqual(childrenFound.Length, children.Length);
            for (int i = 0; i < children.Length; i++)
            {
                Assert.AreSame(childrenFound[i], children[i].transform);
            }

            Object.Destroy(grandparent);
            Object.Destroy(parent);
            for (int i = 0; i < children.Length; i++)
            {
                Object.Destroy(children[i]);
            }
        }

        [Test]
        public void FindChildrenContaining_FindsAll_DisabledChildrenByAParcialName()
        {
            const string nameChildToLookFor = "IAmAChild-";

            GameObject grandparent = new GameObject("Grandparent");
            GameObject parent = new GameObject("Parent");
            GameObject[] children = new GameObject[]
            {
                new GameObject(nameChildToLookFor + "0"),
                new GameObject(nameChildToLookFor + "1"),
                new GameObject(nameChildToLookFor + "2"),
            };

            foreach (GameObject child in children)
            {
                child.SetActive(false);
                child.transform.parent = parent.transform;
            }
            parent.transform.parent = grandparent.transform;
            grandparent.transform.parent = transform;

            parent.SetActive(false);
            grandparent.SetActive(false);

            Transform[] childrenFound = transform.FindChildrenContaining(nameChildToLookFor, true);

            Assert.AreEqual(childrenFound.Length, children.Length);
            for (int i = 0; i < children.Length; i++)
            {
                Assert.AreSame(childrenFound[i], children[i].transform);
            }

            Object.Destroy(grandparent);
            Object.Destroy(parent);
            for (int i = 0; i < children.Length; i++)
            {
                Object.Destroy(children[i]);
            }
        }

        [Test]
        public void Find_Finds_AComponentOnTheScene()
        {
            AudioSource addedComponent = new GameObject("AudioSource").AddComponent<AudioSource>();

            AudioSource foundComponent = transform.Find<AudioSource>(false);

            Assert.AreSame(foundComponent, addedComponent);

            Object.Destroy(addedComponent.gameObject);
        }

        [Test]
        public void FindAll_FindsAll_ComponentsOnTheScene()
        {
            AudioSource[] audioSources = new AudioSource[] 
            {
                    new GameObject("AudioSource-00").AddComponent<AudioSource>(),
                    new GameObject("AudioSource-01").AddComponent<AudioSource>(),
                    new GameObject("AudioSource-02").AddComponent<AudioSource>()
            };            

            AudioSource[] audioSourcesFound = transform.FindAll<AudioSource>(false);

            Assert.AreEqual(audioSourcesFound.Length, audioSources.Length);

            for (int i = 0; i < audioSources.Length; i++)
            {
                Object.Destroy(audioSources[i].gameObject);
            }
        }

        [Test]
        public void FindChild_Finds_AChildComponentByHierarchyPath()
        {
            const string nameChildToLookFor = "IAmAChild";

            GameObject grandparent = new GameObject("Grandparent");
            GameObject parent = new GameObject("Parent");
            GameObject child = new GameObject(nameChildToLookFor);

            AudioSource audioSourceChild = child.AddComponent<AudioSource>();

            child.transform.parent = parent.transform;
            parent.transform.parent = grandparent.transform;
            grandparent.transform.parent = transform;

            string pathToLookFor = $"{grandparent.name}/{parent.name}/{child.name}";
            AudioSource audioSourceFound = transform.FindChild<AudioSource>(pathToLookFor);

            Assert.AreSame(audioSourceFound, audioSourceChild);
        }

        [Test]
        public void FindChild_Create_AChildComponentIfNotFound()
        {
            const char separator = '/';
            string pathToLookFor = $"Grandparent{separator}Parent{separator}Child";

            AudioSource addedComponent = transform.FindChild<AudioSource>(pathToLookFor, true);

            string[] childrenNames = pathToLookFor.Split(separator);
            int tranformChilds = transform.hierarchyCount - 1;

            UnityEngine.Assertions.Assert.IsNotNull(addedComponent, "Component was not added to hierarchy.");
            Assert.AreEqual(tranformChilds, childrenNames.Length);

            Transform lastParent = transform;
            for (int i = 0; i < childrenNames.Length; i++)
            {
                lastParent = lastParent.Find(childrenNames[i]);
                UnityEngine.Assertions.Assert.IsNotNull(lastParent, $"Child GameObject {childrenNames[i]} was not added to hierarchy.");
            }
        }

        [Test]
        public void LookAt2D_PointsTransformRightVector_To_TargetPosition()
        {
            transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            transform.localScale = Vector3.one;

            Transform other = new GameObject("OtherTestObject").transform;
            other.SetPositionAndRotation(new Vector3(10f, 5f, 0f), Quaternion.identity);
            
            transform.LookAt2D(other.position, Space.Self);

            Vector3 direction = (other.position - transform.position).normalized;
            float dotProduct = Vector3.Dot(direction, transform.right);
            bool isFacingToOtherObject = Mathf.Approximately(dotProduct, 1F);

            Assert.IsTrue(isFacingToOtherObject);

            Object.Destroy(other.gameObject);
        }

        [Test]
        public void LookAtDirection2D_PointsTransform_To_TargetDirection()
        {
            transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            transform.localScale = Vector3.one;

            Vector3 direction = new Vector3(0.1f, 0.1f, 0f).normalized;

            transform.LookAtDirection2D(direction);

            float dotProduct = Vector3.Dot(direction, transform.right);
            bool isFacingToDirection = Mathf.Approximately(dotProduct, 1F);

            Assert.IsTrue(isFacingToDirection);
        }
    }
}
