﻿using UnityEngine;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace ActionCode.TransformExtension.Tests
{
    public class TranslatorTest
    {
        private Translator translator;

        [SetUp]
        public void SetUp()
        {
            translator = new GameObject("TestObject").AddComponent<Translator>();
            translator.speed = 0f;
            translator.direction = Vector3.zero;
            translator.space = Space.Self;
        }

        [TearDown]
        public void TearDown()
        {
            Object.Destroy(translator.gameObject);
        }

        [Test]
        public void SetSpeedAndDirection_Sets_SpeedAndDirection()
        {
            const float speed = 5f;
            Vector3 direction = Vector3.up;

            translator.SetSpeedAndDirection(speed, direction);

            Assert.AreEqual(speed, translator.speed);
            Assert.AreEqual(direction, translator.direction);
        }

        [Test]
        public void SetSpeedAndDirection_Sets_NegativeSpeedAndDirection()
        {
            const float speed = -5f;
            Vector3 direction = -Vector3.up;

            translator.SetSpeedAndDirection(speed, direction);

            Assert.AreEqual(0F, translator.speed);
            Assert.AreEqual(direction, translator.direction);
        }

        [UnityTest]
        public IEnumerator MovesTransform_ToRight()
        {
            translator.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

            const float speed = 15f;
            Vector3 direction = Vector3.right;
            Vector3 velocity = direction * speed * Time.deltaTime;
            Vector3 resultPosition = translator.transform.position + velocity;

            translator.speed = speed;
            translator.direction = direction;

            yield return null;

            Assert.GreaterOrEqual(resultPosition.x, resultPosition.x);
            Assert.AreEqual(resultPosition.y, translator.transform.position.y);
            Assert.AreEqual(resultPosition.z, translator.transform.position.z);
        }
    }
}
